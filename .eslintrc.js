module.exports = {
	extends: 'airbnb',
	parserOptions: {
		'ecmaVersion': 2018
	},
	env: {
		es6: true,
		browser: true,
		node: true,
		mocha: true
	},
	rules: {
		'import/newline-after-import': 0,
		'indent': [2, 'tab'],
		'no-tabs': 0,
		'no-unused-vars': 'warn',
		'no-console': 'error',
		'consistent-return': 0,
		'strict': 0,
		'padded-blocks': 0,
		'prefer-destructuring': 0,
		'class-methods-use-this': 0,
		'comma-dangle': 0,
		'no-underscore-dangle': 0,
		'no-use-before-define': 0,
		'arrow-parens': 0,
		'no-plusplus': 0,
		'max-len': [2, 120, 4],
		'no-restricted-globals': 0,
		'no-await-in-loop': 0,
		'func-names': 0,
		'operator-linebreak': 0,
	}
};

# Simple WebSocket game
Uses websocket commands to communicate between client and server.

## Instructions

__Prerequisites__

* Node 9.8.0+
* NPM 5.6.0+
* Tested only on Mac machine, theoretically Windows issues are possible although unlikely.

__Usage__

* `npm install` - install all dependencies.
* `npm test` - run all tests.
* `npm run start:dev-w` - start "DEV" server.

__Testing Game__

It's just WebSockets - connect to `ws://localhost:8080/api/game` some suggestions:

* `Smart Websocket Client` Chrome extension - seems to work alright.
* `npm run start-client` - start WebSocket client to test running server - not good though, jumps to new line, hard to type.

__Game API__

* Send `HELP` message to get further on-connection instructions.

# Architecture

Project is structured by kinda `Package by Component` philosophy - there are `components` that are "kinda" Microservice-ready like and can mange themselves (and include internal implementation details) and there's top `API` layer.

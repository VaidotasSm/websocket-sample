'use strict';

const bunyan = require('bunyan');
const config = require('config');

let logConfig = config.app.log;
if (config.app.log.stream === 'process.stdout') {
	logConfig = { ...logConfig, stream: process.stdout };
}

exports.logger = bunyan.createLogger(logConfig);

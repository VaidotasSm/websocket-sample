'use strict';

const express = require('express');
const config = require('config');
const { WinterIsComingProcess } = require('../components/winter-is-coming');

const gameProcess = new WinterIsComingProcess(config.game.defaultNotifyPeriod);

module.exports = function gameRouter() {
	const router = express.Router();

	router.ws('/', (ws) => {
		let context = {
			channel: {
				update(msg) {
					ws.send(msg);
				}
			}
		};
		ws.on('message', async (msg) => {
			try {
				const result = await gameProcess.runCommand(msg, context);
				context = result.context;
				ws.send(result.message);
			} catch (err) {
				ws.send(`ERROR: ${err.message}`);
			}
		});
		ws.on('close', () => {
			// Ensure no updates are pushed
			context.channel.update = () => undefined;
		});
	});

	return router;
};

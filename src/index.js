'use strict';

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const config = require('config');
const ExpressWS = require('express-ws');
const { logger } = require('./logger');
const gameRouter = require('./routes/game');

const app = express();
ExpressWS(app);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/game', gameRouter());

app.use((req, res) => {
	res.status(404).json({ message: 'Not found' });
});
app.use((err, req, res, next) => {
	if (!err) {
		return next();
	}
	res.status(500).json({ message: 'Something went wrong' });
});

app.listen(config.app.port, () => {
	logger.info('Started...');
});

module.exports = app;

'use strict';

const uuid = require('uuid');
const { logger } = require('../../logger');
const { WinterIsComingGame } = require('./engine');

// Should be more sophisticated in production - e.g. Redis
const simpleGameCache = {
	games: {},
	put(gameId, game, channel, interval) {
		this.games[gameId] = {
			gameId,
			game,
			channel,
			interval
		};
	},
	get(gameId) {
		return this.games[gameId];
	},
	remove(gameId) {
		delete this.games[gameId];
	},
	removeAll() {
		this.games = {};
	}
};

/**
 * Game Process that accepts and runs text commands
 *
 */
exports.WinterIsComingProcess = class WinterIsComingProcess {
	constructor(notifyPeriod = 2000, gameCache = simpleGameCache) {
		this._notifyPeriod = notifyPeriod;
		this._gameCache = gameCache;
	}

	/**
	 * Run Game Command
	 *
	 * @param {string} command - game command
	 * @param {[gameId], [channel]}} context - additional environment info
	 * @returns {{message: string, context}} command result
	 */
	async runCommand(command, context) {
		try {
			if (command.startsWith('HELP')) {
				return Resp('Available commands: "START {player}", "SHOOT {x} {y}"', context);
			}

			if (command.startsWith('START ')) {
				const [, playerName] = command.split(' ');
				return this._startGame(playerName, context, command);
			}

			if (command.startsWith('SHOOT ') || command.startsWith('S ')) {
				const [, x, y] = command.split(' ');
				return this._playerShoot({ x, y }, context, command);
			}

			throw new WinterIsComingError('Invalid command - try "HELP"', context, command);
		} catch (err) {
			logger.error(err);
			if (err instanceof WinterIsComingError) {
				return Resp(`ERROR: ${err.message}`, context);
			}
			return Resp(`ERROR: something went wrong - ${err.message}`, context);
		}
	}

	_startGame(playerName, context, command) {
		if (!playerName) {
			throw new WinterIsComingError('should provide player name - "START {player}', context, command);
		}
		if (!context.channel || !context.channel.update) {
			throw new WinterIsComingError('No communication channel is provided', context, command);
		}
		if (context.gameId) {
			throw new WinterIsComingError('cannot start second game', context, command);
		}

		const game = new WinterIsComingGame(playerName);
		game.start();

		const interval = setInterval(() => {
			// Update status
			if (game.finished) {
				this._finishGame(this._gameCache.get(gameId));
				return;
			}

			// Move zombies
			game.zombiesMove()
				.map(({ name, position }) => `WALK ${name} ${position.x} ${position.y}`)
				.forEach((msg) => context.channel.update(msg));
		}, this._notifyPeriod);

		const gameId = uuid.v4();
		this._gameCache.put(gameId, game, context.channel, interval);

		return Resp(`Game started id=${gameId}`, { ...context, gameId });
	}

	_playerShoot({ x, y }, context, command) {
		if (!context.gameId) {
			throw new WinterIsComingError('No game ID is provided', context, command);
		}
		const cache = this._gameCache.get(context.gameId);
		if (!cache || !cache.game || !cache.channel) {
			throw new WinterIsComingError(
				'No such game, try starting, or connecting to one', context, command
			);
		}
		if (!x || !y || !isFinite(x) || !isFinite(y)) {
			throw new WinterIsComingError('SHOOT must have x and y coordinates', context, command);
		}
		if (cache.game.finished) {
			throw new WinterIsComingError('game is finished', context, command);
		}

		const hits = cache.game.playerShoot({ x: parseInt(x, 10), y: parseInt(y, 10) });

		if (cache.game.finished) {
			this._finishGame(cache);
		}
		return Resp(`BOOM ${cache.game.playerName} ${hits.length} ${hits.join(', ')}`, context);
	}

	_finishGame(cache) {
		// Send update
		cache.channel.update(
			'Game is finished - ' +
			`${cache.game.winner.name} (${cache.game.winner.isPlayer ? 'player' : 'zombie'}) has won`
		);

		setTimeout(() => {
			// Clear timer
			clearInterval(cache.interval);

			// Clear cache
			this._gameCache.remove(cache.gameId);
		}, this._notifyPeriod);
	}
};

function Resp(message, context) {
	return { message, context };
}

class WinterIsComingError {
	constructor(message, context, command) {
		this.message = message;
		this.context = context;
		this.command = command;
	}
}
exports.WinterIsComingError = WinterIsComingError;

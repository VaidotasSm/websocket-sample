'use strict';

module.exports = class BoardManager {
	/**
	 * @constructor
	 * @param {{x: number, y: number}} boardSize - size of the board
	 */
	constructor(boardSize, randomizer = Math.random) {
		this.randomizer = randomizer;
		if (!boardSize) {
			throw new Error('Must provide board size');
		}
		const { x, y } = boardSize;
		if (!x || !y) {
			throw new Error('boardSize must contain x and y');
		}
		this.boardSize = { x, y };
	}

	/**
	 * Calculate new position of object
	 *
	 * @param {{x: number, y: number}} currentPosition - current object position
	 * @returns {{position: object, hasReachedEnd: boolean}} new position info
	 */
	makeMove(currentPosition) {
		if (!currentPosition || !currentPosition.x || !currentPosition.y) {
			throw new Error('Must provide currentPosition');
		}

		const randomX = Math.floor(this.randomizer() * 3) - 1;
		const newPosition = {
			x: validX(currentPosition.x + randomX, this.boardSize.x),
			y: validY(currentPosition.y + 1)
		};
		return {
			position: newPosition,
			hasReachedEnd: newPosition.y >= this.boardSize.y
		};
	}
};

function validX(x, maxX) {
	if (x <= 0) {
		return 1;
	}
	if (x > maxX) {
		return maxX - 1;
	}
	return x;
}

function validY(y, maxY) {
	if (y <= 0) {
		return 1;
	}
	if (y > maxY) {
		return maxY;
	}
	return y;
}

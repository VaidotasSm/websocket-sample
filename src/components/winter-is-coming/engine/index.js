'use strict';

const BoardManager = require('./boardManager');

/**
 * Game:
 * Board of 10x30 cells
 * One side - has Zombies
 * Other side - has Wall (with archer on it)
 */
exports.WinterIsComingGame = class WinterIsComingGame {
	constructor(playerName, zombieNames = ['night-king'], boardSize = { x: 10, y: 30 }) {
		this.started = false;
		this.finished = false;
		this.winner = null;
		this.playerName = playerName;
		this.boardManager = new BoardManager(boardSize);

		this.zombies = {};
		zombieNames.forEach((name) => {
			this.zombies[name] = Position(Math.ceil(Math.random() * boardSize.x), 1);
		});
	}

	/**
	 * Start new game
	 */
	start() {
		this.started = true;
	}

	zombiesPosition() {
		return Object.keys(this.zombies).map((zombieName) => this._getZombiePositionOverview(zombieName));
	}

	zombiesMove() {
		this._requireValidGameState();

		return Object.keys(this.zombies).map((zombieName) => {
			const { position, hasReachedEnd } = this.boardManager.makeMove(this.zombies[zombieName]);
			this.zombies[zombieName] = position;
			if (hasReachedEnd) {
				this.finished = true;
				this.winner = Winner(zombieName, position, false);
			}
			return this._getZombiePositionOverview(zombieName);
		});
	}

	playerShoot({ x, y }) {
		this._requireValidGameState();

		if (typeof x !== 'number' || typeof y !== 'number') {
			throw new Error('x and y coordinates must be numbers');
		}

		const hitZombies = this.zombiesPosition()
			.filter(({ position }) => x === position.x && y === position.y)
			.map(({ name }) => name);

		// Remove zombies
		hitZombies.forEach((name) => {
			delete this.zombies[name];
		});

		// Have Winner?
		if (this.zombiesPosition().length === 0) {
			this.finished = true;
			this.winner = Winner(this.playerName, null, true);
		}
		return hitZombies;
	}

	_getZombiePositionOverview(zombieName) {
		return {
			name: zombieName,
			position: this.zombies[zombieName]
		};
	}

	_requireValidGameState() {
		if (!this.started) {
			throw new Error('Game has not started');
		}
		if (this.finished) {
			throw new Error('Game has already finished');
		}
	}
};

function Position(x = 0, y = 0) {
	return { x, y };
}

function Winner(name, position, isPlayer) {
	return {
		name,
		position,
		isPlayer
	};
}

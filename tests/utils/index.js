'use strict';

const { assert } = require('chai');

exports.shouldThrow = function shouldThrow(callback, message = 'should fail') {
	let err;
	try {
		callback();
	} catch (e) {
		err = e;
	}
	assert.exists(err, message);
	return err;
};

exports.shouldThrowAsync = async function shouldThrow(callback, message = 'should fail') {
	let err;
	try {
		await callback();
	} catch (e) {
		err = e;
	}
	assert.exists(err, message);
	return err;
};

exports.wait = function wait(milliseconds) {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(true);
		}, milliseconds);
	});
};

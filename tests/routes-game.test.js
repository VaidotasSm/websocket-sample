'use strict';

const { assert } = require('chai');
const WebSocketClient = require('websocket').client;
const utils = require('./utils');

// Start server!
require('../src/index');

describe('game websocket endpoint >', function () {
	this.timeout(3000);

	it('should send/receive to/from game WS endpoint', async () => {
		const receivedMessages = [];
		const client = new WebSocketClient();
		client.on('connect', (connection) => {
			connection.on('message', (message) => {
				receivedMessages.push(message.utf8Data);
			});

			send(connection, 'HELP');
		});
		client.connect(
			'ws://localhost:8080/api/game',
			'echo-protocol'
		);

		await utils.wait(1500);
		assert.deepInclude(receivedMessages, 'Available commands: "START {player}", "SHOOT {x} {y}"');
	});
});

function send(connection, command) {
	if (connection.connected) {
		connection.sendUTF(command);
	}
}

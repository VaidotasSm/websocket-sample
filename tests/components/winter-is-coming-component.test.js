'use strict';

const { assert } = require('chai');
const { wait } = require('../utils');
const { WinterIsComingProcess } = require('../../src/components/winter-is-coming');

const process = new WinterIsComingProcess(250);

describe('Winter is coming component >', () => {
	afterEach(async () => {
		// Clear game cache
		process._gameCache.removeAll();
	});

	it('should fail with unknown commands', async () => {
		const { message, context } = await process.runCommand('UNKNOWN X Y', {});
		assert.equal(message, 'ERROR: Invalid command - try "HELP"');
		assert.exists(context, 'should return context');
	});

	it('should support HELP command', async () => {
		const { message } = await process.runCommand('HELP', {});
		assert.equal(message, 'Available commands: "START {player}", "SHOOT {x} {y}"');
	});

	describe('START command >', () => {
		it('should fail without player name', async () => {
			const { message } = await process.runCommand('START ', {});
			assert.equal(message, 'ERROR: should provide player name - "START {player}');
		});

		it('should fail when no channel provided', async () => {
			const res = await process.runCommand('START player1', {});
			assert.equal(res.message, 'ERROR: No communication channel is provided');
		});

		it('should return context', async () => {
			const res = await process.runCommand('START player1', {});
			assert.exists(res.context);
		});

		it('should start the game', async () => {
			const { message, context } = await process.runCommand('START player1', { channel: MockChannel() });
			assert.include(message, 'Game started id=');

			const game = process._gameCache.get(context.gameId);
			assert.exists(game, 'should cache new game');
		});

		it('should get zombie walks', async () => {
			const channel = MockChannel();
			await process.runCommand('START player1', { channel });

			await wait(500);
			assert.isAtLeast(channel.updates.length, 1, 'should send updates to channel');
		});

		it('should not allow to start second game withing session', async () => {
			const channel = MockChannel();
			const { context } = await process.runCommand('START player1', { channel });

			const { message } = await process.runCommand('START player2', context);
			assert.equal(message, 'ERROR: cannot start second game', 'should fail to start 2nd game');
		});
	});

	describe('SHOOT command >', async () => {
		it('should not work when no game ID provided', async () => {
			const { message, context } = await process.runCommand('SHOOT 1 1', {});
			assert.equal(message, 'ERROR: No game ID is provided');
			assert.exists(context, 'should return context');
		});

		it('should not work when no game exists', async () => {
			const { message, context } = await process.runCommand('SHOOT 1 1', { gameId: 'game-1' });
			assert.equal(message, 'ERROR: No such game, try starting, or connecting to one');
			assert.exists(context, 'should return context');
		});

		it('should not work when no x/y coordinates', async () => {
			const channel = MockChannel();
			let res = await process.runCommand('START player1', { channel });
			const gameId = res.context.gameId;

			res = await process.runCommand('SHOOT ', { gameId });
			assert.equal(res.message, 'ERROR: SHOOT must have x and y coordinates');

			res = await process.runCommand('SHOOT 1', { gameId });
			assert.equal(res.message, 'ERROR: SHOOT must have x and y coordinates');

			res = await process.runCommand('SHOOT a b', { gameId });
			assert.equal(res.message, 'ERROR: SHOOT must have x and y coordinates');
		});

		it('should get correct response', async () => {
			const channel = MockChannel();
			const res1 = await process.runCommand('START player1', { channel });
			const res2 = await process.runCommand('SHOOT 1 1', { gameId: res1.context.gameId });
			assert.include(res2.message, 'BOOM player');
			assert.exists(res2.context, 'should return context');
		});

	});

	describe('full scenarios >', () => {
		it('should inform about player won game', async () => {
			const processLong = new WinterIsComingProcess(9999);
			const channel = MockChannel();
			const res1 = await processLong.runCommand('START player1', { channel });
			const gameId = res1.context.gameId;

			const responses = [];
			try {
				for (let x = 1; x <= 10; x++) {
					responses.push(await processLong.runCommand(`SHOOT ${x} 1`, { gameId }));
				}
			} catch (err) {
				// ignore
			}

			assert.deepInclude(
				responses.map((r) => r.message),
				'BOOM player1 1 night-king',
				'should receive response about hit'
			);
			assert.deepInclude(channel.updates, 'Game is finished - player1 (player) has won');
		});

		it('should inform about zombie won game', async () => {
			const processLong = new WinterIsComingProcess(10);
			const channel = MockChannel();
			await processLong.runCommand('START player1', { channel });

			await wait(1500);
			assert.isAtLeast(channel.updates.length, 30, 'should send updates about finished game');
			assert.deepInclude(channel.updates, 'Game is finished - night-king (zombie) has won');
		});
	});

});

function MockChannel() {
	return {
		updates: [],
		update(msg) {
			this.updates.push(msg);
		}
	};
}

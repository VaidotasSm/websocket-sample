'use strict';

const { assert } = require('chai');
const { WinterIsComingGame } = require('../../../src/components/winter-is-coming/engine');
const { shouldThrow } = require('../../utils');

describe('Winter is Coming game test >', () => {
	describe('Initialization >', () => {
		it('should fail when game is not started', () => {
			const game = new WinterIsComingGame('Player1');
			shouldThrow(() => game.zombiesMove(), 'should throw - zombiesMove()');
			shouldThrow(() => game.playerShoot({ x: 0, y: 0 }), 'should throw - playerShoot()');
		});
	});

	describe('zombie moves >', () => {
		it('should set correct initial position', () => {
			const game = new WinterIsComingGame('Player1', ['Zombie1'], { x: 10, y: 20 });
			game.start();

			const [{ position }] = game.zombiesPosition();
			assert.equal(position.y, 1);
			assert.isAtLeast(position.x, 1);
			assert.isAtMost(position.x, 10);
		});

		it('should return correct data', () => {
			const game = startGame();

			const moves = game.zombiesMove();
			assert.equal(moves.length, 1, 'should make 1 move per zombie');

			const { name, position } = moves[0];
			assert.equal(name, 'Zombie1');
			assert.isAtLeast(position.x, 1);
			assert.isAtLeast(position.y, 1);
		});

		it('should move zombie to correct direction', () => {
			const game = startGame();

			while (!game.finished) {
				const [{ position: oldPosition }] = game.zombiesPosition();
				const [{ position }] = game.zombiesMove();
				assert.isAbove(position.y, oldPosition.y, 'should move forward - y');
				assert.isAtMost(oldPosition.x - position.x, 1, 'should not fly to other square - x');
			}
		});

		it('should not move zombie outside the board', () => {
			const boardSize = { x: 4, y: 4 };
			const game = startGame(['Zombie1'], boardSize);

			while (!game.finished) {
				const [{ position }] = game.zombiesMove();
				assertIsInsideBoard(position, boardSize);
			}
		});

		describe('multiple zombies >', () => {
			it('should try to start zombies in randomized order', () => {
				const game = startGame(['z1', 'z2', 'z3', 'z4', 'z5', 'z6', 'z7', 'z8'], { x: 10, y: 4 });

				const positions = game.zombiesPosition();
				assert.equal(positions.length, 8, 'should set up all zombies');

				const uniqueX = new Set(positions.map(({ position }) => position.x));
				assert.isAbove(uniqueX.size, 2, 'should not start on same square all the time');
			});

			it('should move multiple zombies', () => {
				const boardSize = { x: 4, y: 4 };
				const game = startGame(['z1', 'z2', 'z3'], boardSize);

				while (!game.finished) {
					const moves = game.zombiesMove();
					assert.equal(moves.length, 3, 'should move all zombies');
					assertIsInsideBoard(moves[0].position, boardSize);
					assertIsInsideBoard(moves[1].position, boardSize);
					assertIsInsideBoard(moves[2].position, boardSize);
				}
			});
		});

		describe('game ends >', () => {
			it('should not allow to continue the game', () => {
				const game = startGame(['Zombie1'], { x: 4, y: 4 });

				while (!game.finished) {
					game.zombiesMove();
				}

				shouldThrow(() => game.zombiesMove());
				shouldThrow(() => game.playerShoot({ x: 1, y: 1 }));
			});

			it('should save the winner zombie', () => {
				const game = startGame(['Zombie1'], { x: 4, y: 4 });

				while (!game.finished) {
					game.zombiesMove();
				}

				assert.deepInclude(
					game.winner,
					{
						name: 'Zombie1',
						isPlayer: false
					},
					'should set correct winner'
				);
				assert.equal(game.winner.position.y, 4, 'winner zombie should be on the last Y row');
			});
		});
	});

	describe('player shots >', () => {
		it('should report when missed', () => {
			const game = startGame();

			const hits = game.playerShoot({ x: 2, y: 2 });
			assert.deepEqual(hits, [], 'should not hit any zombie');
		});

		it('should hit zombie, when targets its position', () => {
			const game = startGame();

			const [{ position }] = game.zombiesPosition();
			const hits = game.playerShoot(position);
			assert.deepEqual(hits, ['Zombie1'], 'should hit zombie');

			const zombies = game.zombiesPosition();
			assert.deepEqual(zombies, [], 'should remove zombie from game');
		});

		it('should end the game', () => {
			const game = startGame();

			const [{ position }] = game.zombiesPosition();
			game.playerShoot(position);

			assert.isTrue(game.finished, 'should end game');
			assert.deepInclude(
				game.winner,
				{
					name: 'Player1',
					isPlayer: true
				},
				'should set correct winner'
			);
		});

		describe('multiple zombies >', () => {
			it('should hit zombie, when targets its position', () => {
				const game = startGame(['z1', 'z2', 'z3', 'z4', 'z5'], { x: 10, y: 20 });

				const [{ position }] = game.zombiesPosition();
				const hits = game.playerShoot(position);
				assert.isAtLeast(hits.length, 1, 'should hit zombie at least one zombie');

				const zombies = game.zombiesPosition();
				assert.deepEqual(zombies.length, 5 - hits.length, 'should remove zombie from game');
			});

			it('should not end the game when zombies remain', () => {
				const game = startGame(['z1', 'z2', 'z3', 'z4', 'z5'], { x: 10, y: 20 });

				const [{ position }] = game.zombiesPosition();
				game.playerShoot(position);

				assert.isFalse(game.finished, 'should not end game');
				assert.equal(game.winner, null, 'should set correct winner');
			});

			it('should destroy multiple zombies on same square', () => {
				const game = startGame(['z1', 'z2', 'z3', 'z4', 'z5', 'z6', 'z7'], { x: 2, y: 10 });

				const [{ position }] = game.zombiesPosition();
				const hits = game.playerShoot(position);
				assert.isAtLeast(hits.length, 2, 'should hit zombie at least one zombie');

				const zombies = game.zombiesPosition();
				assert.deepEqual(zombies.length, 7 - hits.length, 'should remove zombies from game');
			});
		});
	});
});

function assertIsInsideBoard(position, boardSize) {
	assert.isAtMost(position.y, boardSize.y);
	assert.isAtLeast(position.y, 1);
	assert.isAtMost(position.x, boardSize.x);
	assert.isAtLeast(position.x, 1);
}

function startGame(zombies = ['Zombie1'], boardSize = { x: 4, y: 4 }) {
	const game = new WinterIsComingGame('Player1', zombies, boardSize);
	game.start();

	return game;
}

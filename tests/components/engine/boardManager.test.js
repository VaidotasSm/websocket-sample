'use strict';

const { assert } = require('chai');
const BoardManager = require('../../../src/components/winter-is-coming/engine/boardManager');

describe('makeMove() >', () => {
	const boardSize = { x: 4, y: 4 };

	it('should correctly change X position', () => {
		let manager = createBoardManager(boardSize, 0.99);
		assert.equal(manager.makeMove({ x: 4, y: 2 }).position.x, 3);

		manager = createBoardManager(boardSize, 0.4);
		assert.equal(manager.makeMove({ x: 4, y: 2 }).position.x, 4);

		manager = createBoardManager(boardSize, 0);
		assert.equal(manager.makeMove({ x: 4, y: 2 }).position.x, 3);
	});
});

function createBoardManager(boardSize, randomizerResult) {
	const randomizer = () => randomizerResult;
	const manager = new BoardManager(boardSize, randomizer);
	return manager;
}
